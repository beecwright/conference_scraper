import numpy as np
import urllib.request as req
from bs4 import BeautifulSoup as soup



urls = ['https://link.springer.com/book/10.1007/978-3-030-00928-1',
        'https://link.springer.com/book/10.1007/978-3-030-00928-1?page=2#toc',
        'https://link.springer.com/book/10.1007/978-3-030-00928-1?page=3#toc', # volume 1
        'https://link.springer.com/book/10.1007/978-3-030-00934-2',
        'https://link.springer.com/book/10.1007/978-3-030-00934-2?page=2#toc',
        'https://link.springer.com/book/10.1007/978-3-030-00934-2?page=3#toc', # volume 2
        'https://link.springer.com/book/10.1007/978-3-030-00931-1',
        'https://link.springer.com/book/10.1007/978-3-030-00931-1?page=2#toc',
        'https://link.springer.com/book/10.1007/978-3-030-00937-3',
        'https://link.springer.com/book/10.1007/978-3-030-00937-3?page=2#toc']

all_titles = []
for u in urls:
    rep = req.urlopen(u)
    page = soup(rep, 'html.parser')
    titles = page(attrs={'class' : 'content-type-list__link u-interface-link gtm-chapter-link'})    
    tmp_titles = [t.text for t in titles]
    all_titles.append(tmp_titles)

titles = np.concatenate(np.array(all_titles))

gan = ['generative', 'adversarial', 'gan']
gan_papers = []
for t in titles:
    is_gan_paper = False
    words = t.lower().split(' ')
    for w in words:
        if w in gan:
            is_gan_paper = True
    if is_gan_paper:
        gan_papers.append(t)