'''
Description: Parse NeurIPS pdf papers into their various sections.
Created: 19/05/16
Modified: 19/06/25
Python: 3.52
'''

import numpy as np
import unicodedata
from io import BytesIO as StringIO
from pdfminer.pdfpage import PDFPage
from pdfminer.layout import LAParams
from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter


class PDFParser:
    '''
    Description: Methods for parsing PDF conference papers.        
    '''    
    def __init__(self, path):
        '''
        path (str): absolute file path to a single conference paper
        '''
        self.path = path
        
        
    def convert_pdf_to_txt(self):
        '''
        Description: convert a pdf file into text
        '''
        
        rsrcmgr = PDFResourceManager()
        retstr = StringIO()
        codec = 'utf-8'
        laparams = LAParams()
        device = TextConverter(rsrcmgr, retstr, codec=codec, laparams=laparams)
        fp = open(self.path, 'rb')
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        password = ""
        maxpages = 0
        caching = True
        pagenos=set()

        for page in PDFPage.get_pages(fp, pagenos, maxpages=maxpages, password=password,caching=caching, check_extractable=True):
            interpreter.process_page(page)

        text = retstr.getvalue()

        fp.close()
        device.close()
        retstr.close()
        
        self.text = text
    
    def convert_text_to_ascii(self):
        '''
        Description: decode text using assuming utf-8 encoding, into ascii (ignore anything that doesn't encode into ascii, maybe undesirable)
        '''
        text_unicode = self.text.decode('utf-8')
        clean_output = unicodedata.normalize('NFKD', text_unicode).encode('ascii','ignore')
        
        self.ascii_text = clean_output
        self.ascii_text = self.ascii_text.decode()
        
        
    def clean_text(self, text=None):
        '''
        Description: clean the text before sending it into the vectorizer
        '''
        if text==None:
            text = self.ascii_text
        
        # Remove newline characters and carriage returns
        text = text.strip().replace('\n', ' ').replace('\r', ' ')

        # lowercase
        text = text.lower()

        return text
        
        
    def find_paper_section(self):
        '''
        Description: Find the names of the various sections and their locations in the paper
        '''
        
        # THIS DOESN'T FULLY WORK YET
        for i in range(10):
            idx = self.ascii_text.find('\n' + str(i) + ' ')
            if idx == -1:
                print(i, idx)
            else:
                print(pdf.ascii_text[idx: idx+15])
        
        
    def get_abstract(self, text=None):
        '''
        Description: Extract the abstract and introduction sections
        '''
        
        if text == None:
            text = self.ascii_text
        
        # Get the beginning of the Abstract and the Introduction
        abstract_estimated_start = text.lower().find('abstract')
#         abstract_estimated_end = text.find('\nIntroduction')
        
#         # If there is no newline character, try to find the abstract without it
#         if abstract_estimated_end < abstract_estimated_start:
#             abstract_estimated_end = text.lower().find('introduction')
            
#         # If teh value for the intro is super high, its not actually the intro
#         # Just a mention of the word 'introduction'
#         # Instead use the numeric section heading to try and find the intro
#         if 2000 < len(text[abstract_estimated_start:abstract_estimated_end].split()):
#             abstract_estimated_end = text.lower().find('\n1 ')            
            
            
        type_1 = text.find('\nIntroduction')
        type_2 = text.lower().find('introduction')
        type_3 = text.lower().find('\n1 ')

        all_types = np.array([type_1, type_2, type_3])
        abstract_estimated_end = np.min(all_types[all_types > 0])
                    
        # If we still can't find the abstract, return a failed abstract
        if abstract_estimated_end < abstract_estimated_start:
            self.abstract = 'failed'
            
        # Index the estimated Abstract
        estimated_abstract = text[abstract_estimated_start:abstract_estimated_end]

        # Don't include the word Abstract in the extracted segment
        estimated_abstract = estimated_abstract.split('Abstract')[-1]

        # Find the end of the last sentence in the Abstract, this is the actual end
        end_abstract = estimated_abstract.rfind('.')

        # Clean the abstract
        self.abstract = estimated_abstract[:end_abstract]
        
        self.abstract = self.abstract.strip()