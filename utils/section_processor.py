'''
Description: Process NeurIPS papers by years, aggregating and saving the results
Created: 19/06/10
Modified: 19/06/10
'''


import string
import os, sys
import numpy as np
import pandas as pd
import multiprocessing
from collections import Counter
import matplotlib.pyplot as plt
from nltk.corpus import stopwords


class SectionProcessor:
    def __init__(self, year):
        '''
        Description:
        ------------
        Methods for processing extracted sections from conference papers
        
        Params:
        -------
        year (str): which year of papers to process        
        '''
        self.year = year
    
    
    def accumulate_all_words_in_section(self, df, section):
        '''
        Description:
        ------------
        Accumulate all words in a given titles section of the paper
        
        
        Params:
        ------
        df (pd.DataFrame): contains columns of titles sections from the papers that have already been extracted
        section (str): which section to accumulate
        '''
        all_words = ''
        for i in range(len(df)):
            all_words = all_words + df.iloc[i][section]
        return all_words
    
    
    def make_lower(self, all_words):
        '''
        Description:
        ------------
        Make all words lowercase.
        '''
        all_words = [word.lower() for word in all_words]
        return all_words

    
    def remove_punctuation(self, all_words):
        '''
        Description:
        ------------
        Remove all the puncuation from a given blob of text
        '''
        translator = str.maketrans('', '', string.punctuation)
        all_words = [word.translate(translator) for word in all_words]
        return all_words

    
    def remove_stopwords(self, all_words):
        '''
        Description:
        ------------
        Remove all stopwords from a given blob of text.
        '''        
        filtered_words = [word for word in all_words if word not in stopwords.words('english')]
        return filtered_words 

    
    def restrict_by_freq(self, labels, values, restrict=15):
        '''
        Description:
        ------------
        Threshold on the frequency of a word for inclusion
        
        Params:
        -------
        labels (list): bag of words
        values (nd.array) counts/frequency of the words        
        '''        
        values = values[values > restrict]
        labels = labels[:len(values)]
        return labels, values

    
    def remove_patterns(self, all_words):
        '''
        Description:
        ------------
        Remove certain words, patterns, and words less than 2 characters long.
        '''
        
        patterns = ['et', 'al', 'one', 'two', 'three', 'work', 'show', 'using', 'set', 'use']
        
        # Remove short words
        filtered_words = [word for word in all_words if len(word) > 2]
        
        # Remove words that are in the patterns for removal
        filtered_words = [word for word in filtered_words if word not in patterns]
        return filtered_words
    
    
    def count_occurrance(self, all_words):
        '''
        Description:
        ------------
        Count the frequency of words        
        '''
        counts = Counter(all_words)
        labels, values = zip(*counts.items())

        # sort values in descending order
        indSort = np.argsort(values)[::-1]

        # rearrange data
        labels = np.array(labels)[indSort]
        values = np.array(values)[indSort]

        return labels, values    