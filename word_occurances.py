import string
import numpy as np
import pandas as pd
import nltk.sentiment.util as negate
from collections import Counter
from nltk.corpus import stopwords
import matplotlib.pyplot as plt
from preprocess_reports import preprocess

def get_all_words(df):
    all_words = []
    list_words = df['words']
    for l in list_words:
        all_words = np.concatenate((all_words, l))
        
    return all_words
    
def negate_findings(df):
    all_negated = []
    all_k = []
    count = -1
    for d in range(len(df)):
        sentences = df.iloc[d]['impression'].split('.')
        count += 1
        for s in sentences:
            negated = negate.mark_negation(s.split())
            all_negated.append(string.join(negated))
            
            if count == 0:
                k = all_negated[0]
                
        for i in all_negated[1:]:
            k = k+i
        all_k.append(k)
    df['negated'] = all_k
    return df

def get_all_negated(df):
    all_words = []
    list_words = df['negated']
    for l in list_words:
        print(l)
        all_words = np.concatenate((all_words, l))     
    return all_words
    
def find_negated_phrase(df):
    for i in range(len(df)):
        negated_words = string.join(df.iloc[i]['negated'])
        
        if 'fracture_NEG' in negated_words:
            print(i)
            

def count_occurrance(all_words):
    counts = Counter(all_words)
    labels, values = zip(*counts.items())
    
    # sort values in descending order
    indSort = np.argsort(values)[::-1]
    
    # rearrange data
    labels = np.array(labels)[indSort]
    values = np.array(values)[indSort]

    return labels, values

def make_lower(all_words):
    all_words = [word.lower() for word in all_words]
    return all_words

def remove_punctuation(all_words):
    all_words = [word.translate(None, string.punctuation) for word in all_words]
    return all_words

def remove_stopwords(all_words):
    filtered_words = [word for word in all_words if word not in stopwords.words('english')]
    return filtered_words 

def restrict_by_freq(labels, values, restrict=15):
    values = values[values > restrict]
    labels = labels[:len(values)]
    return labels, values


def plot_hist_freq(labels, values):
    indexes = np.arange(len(labels))


    plt.figure(figsize=(10,10))
    plt.bar(indexes, values, align='center')
    
    # add labels
    plt.xticks(indexes, labels, rotation=45)
    plt.show()


df = pd.read_pickle('/mnt/isilon-bw800/Trauma/data/external/fractures4annotation_postEPIC.pkl')

df = preprocess(df) # extract acc, mrn, report, impression, remove normals
df = negate_findings(df)
#all_words = get_all_negated(df)


#all_words = get_all_words(df) # also remove punctuation
#all_words = make_lower(all_words)
#all_words = remove_punctuation(all_words)
#all_words = remove_stopwords(all_words)
#labels, values = count_occurrance(all_words)
#labels_r, values_r = restrict_by_freq(labels, values, 100)
#
#nrange = range(1,30)
#radial_set = ['comminuted', 'intraarticular', 'impacted', 'dorsally', 'angulated',
#              'displaced' , 'nondisplaced']
#
#plot_hist_freq(labels_r[nrange], values_r[nrange])

