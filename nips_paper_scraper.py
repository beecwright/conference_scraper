import os
import time
import urllib
import numpy as np
import pandas as pd
from pyprind import ProgBar
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt


def make_nips_top_urls(base_url, year_start, year_end, start_idx, end_idx):
    '''
    Description: Create the annual url for paper listings
    '''
    all_urls = []
    idx = list(range(start_idx, end_idx))
    year = list(range(year_start, year_end))
    for i,y in zip(idx, year):
        all_urls.append(base_url + '-' + str(i) + '-' + str(y))
    return all_urls


def get_all_papers_on_page(url):
    '''
    Description: Get all the paper listed on an annual url papers listings
    '''
    
    # Make a url request
    resp = urllib.request.urlopen(url)
    soup = BeautifulSoup(resp, from_encoding=resp.info().get_param('charset'))

    # Parse all links and accumulate the ones that are for papers
    pdf_links = []
    for a in soup.find_all('a', href=True):
        if 'paper' in a['href']:
            pdf_links.append(a['href'])
    
    return pdf_links


def download_file(paper_url, url_reamble, url_postamble, save_path):
    '''
    Description: Download a single nips paper
    '''
    
    # Get the pdf from the web
    response = urllib.request.urlopen(url_preamble + paper_url + url_postamble)    
    
    # Create the save name for the paper
    paper_name = os.path.join(save_path, paper_url.split('/paper/')[1] + '.pdf')
    
    file = open(paper_name, 'wb')
    file.write(response.read())
    file.close()
    
    
if __name__ == '__main__':
    
    # Get all the paper pdf links for a particular neurips page
    start_year = 1988
    end_year = 2019
    start_idx = 1
    end_idx = 31
    url_preamble = 'https://papers.nips.cc'
    url_postamble = '.pdf'    
    
    all_urls = make_nips_top_urls('https://papers.nips.cc/book/advances-in-neural-information-processing-systems', start_year, end_year, start_idx, end_idx)
    
    # Scrap all nips years
    for nips_url in all_urls:
        pdf_links = get_all_papers_on_page(nips_url)

        # Create a save location for this years papers
        year = nips_url.split('-')[-1]
        save_path = '/home/boss/data/nips_papers/' + year
        os.mkdir(save_path)

        # Loop over all pdf paper links and download them
        bar = ProgBar(len(pdf_links))
        for paper_url in pdf_links:
            try:
                time.sleep(0.3)   # sleep so that neurips dones't get pissed at us 
                download_file(paper_url, url_preamble, url_postamble, save_path)
                bar.update()
            except Exception as ex:
                print(ex)    
    
    